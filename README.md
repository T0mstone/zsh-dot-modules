These are some zsh files I use for my zshrc.

They all have no dependencies,
so they can be included simply as
```zsh
for f in "<this directory>"/*.zsh
do
	source "$f"
done
```

## License

A portion of `histpref.zsh` is taken from an MIT-licensed repository
and labelled with the appropriate Copyright notice.
All other code is licensed MIT-0, see `LICENSE`.
