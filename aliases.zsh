is-installed() {
	(which -p "$1" >/dev/null)
}

if is-installed git
then
	alias ga="git add"
	alias gbr="git branch"
	alias gc="git commit"
	alias gcA="git commit --amend"
	alias gd="git diff"
	alias gds="git diff --staged"
	alias gf="git fetch"
	alias gfa="git fetch --all"
	alias gp="git push"
	alias gpf="git push --force-with-lease"
	alias gP="git pull"
	alias gPS="git pull --autostash"
	alias gst="git status"
	alias gsw="git switch"
	alias gS="git stash"
	alias gl="git log --oneline"
	alias glg="git log --oneline --graph"
fi

pdfman() {
	local pdf_command="${PDF_COMMAND:-xdg-open}"
	local _args=`printf '%s' "$*" | sed 's/\x00//g' | sed 's:/:_:g'`
	local _tmp=`mktemp --tmpdir "man $_args.XXXX"`
	man -T pdf "$@" >"$_tmp" \
	&& "$pdf_command" "$_tmp" >/dev/null 2>/dev/null &!
}
alias man=pdfman

if is-installed bat
then
	export BAT_THEME="TwoDark"
	alias cat="bat -p"
fi

# exa is now eza, but still allow fallback if only exa is installed
for exa_ in eza exa
do
	if is-installed $exa_
	then
		alias ls="$exa_ --color=never"
		alias tree="$exa_ --color=never --tree"
		break
	fi
done
unset exa_

if is-installed fd
then
	alias fd="fd --color=never"
fi

if is-installed fswatch
then
	while-watch() {
		fswatch -0 "$@[1,-2]" |  while read -d '' event; do eval "$@[-1]"; done
	}
fi