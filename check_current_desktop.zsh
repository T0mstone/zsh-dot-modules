# a tool to see which windows a given command has on the current desktop
# (e.g. to pass a new-window flag if it is 0)

current_desktop() {
	wmctrl -d | awk '$2 == "*" { print $1 }'
}

# ARGS: <desktop id> <the program to check for>
command_windows_on_desktop() {
	if [[ $# -lt 2 ]]
	then
		echo "USAGE: $0 <desktop id> <executable path>" >&2
		return 1
	fi
	local wid pid
	for wid pid in $(wmctrl -lp | awk '$2 == "'"$1"'" { print $1; print $3 }')
	do
		local exe=`readlink /proc/$pid/exe`
		if [[ "$exe" == "$2" ]]
		then
			echo "$wid"
		fi
	done
}