ensure_in_path() {
	if [[ -z "$1" ]]
	then
		return
	else
		[[ $path[(Ie)$1] -eq 0 ]] && export PATH="$1:$PATH"
	fi
}