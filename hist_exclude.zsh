typeset -a HIST_EXCLUDE=()

zshaddhistory() {
	# trim everything after \n, then everything after ' ' from the input line
	local cmd="${${1%%$'\n'*}%% *}"
	# if either PAUSE_HISTORY is set or the command is in HIST_EXCLUDE,
	# then don't add this command to the history
	if ((${+PAUSE_HISTORY} + $HIST_EXCLUDE[(Ie)$cmd]))
	then
		return 1
	else
		return 0
	fi
}

pause_history() {
	typeset -g PAUSE_HISTORY
}
# don't want pause_history itself to show up in the history
HIST_EXCLUDE+=(pause_history)

continue_history() {
	unset PAUSE_HISTORY
}