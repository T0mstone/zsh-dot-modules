export HISTSIZE=20000
export SAVEHIST=10000

export HISTFILE=~/.zsh_history

## the following history-related options are from https://github.com/voku/dotfiles/tree/397e31eaa57b385bc761a4859d58a65c4f1eb6fd/.redpill/lib/1_options.zsh#L54
## Copyright Lars Moelleken <http://moelleken.org/>
# Allow multiple terminal sessions to all append to one zsh command history
setopt APPEND_HISTORY
# Save each command's beginning timestamp (in seconds since the epoch) and the duration (in seconds) to the history file
setopt EXTENDED_HISTORY
# Add commands as they are typed, don't wait until shell exit
setopt INC_APPEND_HISTORY
# If the internal history needs to be trimmed to add the current command line, setting this option will cause the oldest history event that has a duplicate to be lost before losing a unique event
setopt HIST_EXPIRE_DUPS_FIRST
# Do not enter command lines into the history list if they are duplicates of the previous event
setopt HIST_IGNORE_DUPS
# remove command lines from the history list when the first character on the line is a space
setopt HIST_IGNORE_SPACE
# When searching history don't display results already cycled through twice
setopt HIST_FIND_NO_DUPS
# Do not enter command lines into the history list if they are duplicates of the previous event.
setopt HIST_IGNORE_DUPS
# remove the history (fc -l) command from the history list when invoked
setopt HIST_NO_STORE
# remove superfluous blanks from each command line being added to the history list
setopt HIST_REDUCE_BLANKS
# whenever the user enters a line with history expansion, don't execute the line directly 
setopt HIST_VERIFY