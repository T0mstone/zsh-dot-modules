# ALT+LEFT
bindkey '^[[1;3D' backward-word
# ALT+RIGHT
bindkey '^[[1;3C' forward-word
# DEL
bindkey '^[[3~' delete-char
# CTRL+DEL
bindkey '^[[3;5~' delete-word
# CTRL+LEFT
bindkey '^[[1;5D' beginning-of-line
# CTRL+RIGHT
bindkey '^[[1;5C' end-of-line
# HOME
bindkey '^[[H' beginning-of-line
# END
bindkey '^[[F' end-of-line