# Quality of Life stuff

mcd() {
	# USAGE: mcd <path>
	if [[ $# -eq 0 ]]
	then
		echo "USAGE: mcd <path>" >&2
		return 1
	fi

	if [[ ! -e "$1" ]]
	then
		mkdir -p -- "$1"
	else :
	fi \
	&& cd -P -- "$1"
}

cdu() {
	# USAGE: cdu [<n>]
	if [[ $# -eq 0 ]]
	then
		# set the first argument to "1"
		1=1
	fi

	if (grep -Eq '^[0-9]+$' <<<"$1")
	then
		local i
		for ((i=0;i<$1;i++))
			cd ..
	else
		echo "argument not number: $1" >&2
		echo "USAGE: cdu [<n>]" >&2
	fi
}

doat() {
	# USAGE: doat <dir> <cmd...>
	if [[ $# -le 1 ]]
	then
		echo "USAGE: doat <path> <cmd...>" >&2
		return 1
	fi

	local cwd="$(pwd -P)"

	[[ ! -d "$1" ]] && return 1

	cd "$1" && {
		shift
		"$@"
	}
	cd "$cwd"
}